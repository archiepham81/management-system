import createNextIntlPlugin from "next-intl/plugin";

const withNextIntl = createNextIntlPlugin();

/** @type {import('next').NextConfig} */
const nextConfig = {
  env: {
    NEXTAUTH_URL: "http://localhost:3000",
    NEXTAUTH_URL_INTERNAL: "http://localhost:3000",
    NEXTAUTH_SECRET:"123",
  },
};

export default withNextIntl(nextConfig);
