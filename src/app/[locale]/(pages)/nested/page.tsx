'use client';

import {useTranslations} from 'next-intl';
import PageLayout from '../../../../components/PageLayout';
import AlertDialog from '@/components/AlertDialog';

export default function Home() {
  const t = useTranslations('Nested');

  return (
    <PageLayout title={t('title')}>
      <p>{t('description')}</p>1
      <AlertDialog />
    </PageLayout>
  );
}