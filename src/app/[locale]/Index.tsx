"use client";

import Link from "next/link";
import { Session } from "next-auth";
import { signOut } from "next-auth/react";
import { useLocale, useTranslations } from "next-intl";
import PageLayout from "@/components/PageLayout";
import AlertDialog from "@/components/AlertDialog";
import Dashboard from "@/components/Dashboard";
import { CircularProgress } from "@mui/material";
import { makeStyles } from "@mui/styles";
import { useRouter } from "@/navigation";

const useStyles = makeStyles({
  root: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: "100vh",
  },
});

type Props = {
  session: Session | null;
};

export default function Index({ session }: Readonly<Props>) {
  const t = useTranslations("Index");
  const classes = useStyles();

  return (
    <PageLayout title={t("title")} session={session}>
      {session ? (
        <Dashboard />
      ) : (
        <div className={classes.root}>
          <CircularProgress color="secondary" />
        </div>
      )}
    </PageLayout>
  );
}
