import { Avatar, Skeleton } from "@mui/material";

export default function Loading() {
  // You can add any UI inside Loading, including a Skeleton.
  return (
    <Skeleton variant="circular">
      <Avatar />
    </Skeleton>
  );
}
