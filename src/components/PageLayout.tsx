"use client";
import { ReactNode, useEffect } from "react";
import LocaleSwitcher from "./LocaleSwitcher";
import { Typography } from "@mui/material";
import { Session } from "next-auth";
import { useLocale } from "next-intl";
import { useRouter } from "@/navigation";

type Props = {
  children?: ReactNode;
  title: string;
  session: Session | null;
};

export default function PageLayout({
  children,
  title,
  session,
}: Readonly<Props>) {
  const locale = useLocale();
  const router = useRouter();

  useEffect(() => {
    console.log("session: ", session);
    if (!session) {
      router.replace("/login", { locale: locale });
    }
  }, []);

  return (
    <>
      {children}
      {/* <AlertDialog /> */}
    </>
  );
  // (
  //   <div
  //     style={{
  //       padding: 24,
  //       fontFamily: "system-ui, sans-serif",
  //       lineHeight: 1.5,
  //       boxSizing: "border-box",
  //     }}
  //   >
  //     <div style={{ maxWidth: 510 }}>
  //       <Typography variant="h1" gutterBottom>
  //         {title}
  //       </Typography>
  //       {children}
  //       <div style={{ marginTop: 24 }}>
  //         <LocaleSwitcher />
  //       </div>
  //     </div>
  //   </div>
  // );
}
