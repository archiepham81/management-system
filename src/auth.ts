import { AuthOptions } from 'next-auth';
import CredentialsProvider from 'next-auth/providers/credentials';
import jwt from "jsonwebtoken"

const auth: AuthOptions = {
  providers: [
    CredentialsProvider({
      name: 'Credentials',
      credentials: {
        username: { type: 'text' },
        password: { type: 'password' }
      },
      async authorize(credentials, req) {
        try {
          const rawResponse = await fetch('https://660c32e23a0766e85dbda200.mockapi.io/api/v1/user', {
            method: 'POST',
            body: JSON.stringify({
              username: credentials?.username,
              password: credentials?.password
            })
          });
          const content = await rawResponse.json();
          // throw new Error('no user found');
          return content;
        } catch (error) {
          throw error;
        }
      }
    })
  ],
  jwt: {
    async encode({ secret, token }) {
      // @ts-ignore
      return jwt.sign(token, secret)
    },
    async decode({ secret, token }) {
      // @ts-ignore
      return jwt.verify(token, secret)
    },
  },
  session: {
    strategy: "jwt",
    maxAge: 12 * 60 * 60 * 180,
  },
  secret: process.env.NEXTAUTH_SECRET,
  pages: {
    signIn: "/login",
    error: "/error",
  }
};

export default auth;
